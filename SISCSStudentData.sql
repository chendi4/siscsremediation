DEFINE strmvar = 2218 -- assume freeze for FS21

WITH crs AS (
SELECT rsr.EMPLID ,rsr.ACAD_CAREER,rsr.INSTITUTION ,rsr.strm,sum(c.UNT_BILLING) AS assessed_credits,
count(DISTINCT c.SUBJECT||c.CRSE_CODE) AS course_enrollments
FROM siscs.R_STUDENTTERM_RV rsr 
inner JOIN siscs.R_STUDENTCLASS_RV c 
ON rsr.EMPLID =c.EMPLID 
AND rsr.ACAD_CAREER =c.ACAD_CAREER 
AND rsr.INSTITUTION =c.INSTITUTION 
AND rsr.strm =c.strm
WHERE rsr.strm=&strmvar
AND rsr.PRIMARY_CAR_FLAG ='Y'
GROUP BY rsr.EMPLID ,rsr.ACAD_CAREER,rsr.INSTITUTION ,rsr.strm
),

dis AS (
SELECT *
FROM (
SELECT emplid, lgcy_spcl_need_code
FROM siscs.P_STDNT_GRPS_V a 
INNER JOIN (SELECT stdnt_group, lgcy_spcl_need_code FROM siscs.X_TRACK_STDNTGRPS_RV WHERE conversion_category='SPECIAL NEEDS') b 
ON a.STDNT_GROUP =b.stdnt_group
WHERE a.EDW_ACTV_IND ='Y')
pivot(
count(*) FOR LGCY_SPCL_NEED_CODE  IN ('OS' AS c_special_need_os,'MOBL' AS c_special_need_mobl,
'AC' AS c_special_need_ac,'AU' AS  c_special_need_au, 'BRL' AS c_special_need_brl, 'LP' AS c_special_need_lp,'NONM' AS  c_special_need_nonm)
)
),
eth AS (
SELECT *
FROM (
select distinct		
p_ethnicity_dtl.emplid,		
s_ethnic_grp_tbl.ethnic_group		
from	 siscs.p_ethnicity_dtl_v   p_ethnicity_dtl,	siscs.s_ethnic_grp_tbl_v  s_ethnic_grp_tbl		
where		
p_ethnicity_dtl.ethnic_grp_cd = s_ethnic_grp_tbl.ethnic_grp_cd		
and s_ethnic_grp_tbl.setid = 'USA'		
and s_ethnic_grp_tbl.effdt = (		
select		
max(b_ed.effdt)		
from		
siscs.s_ethnic_grp_tbl_av b_ed		
where		
s_ethnic_grp_tbl.setid = b_ed.setid		
and s_ethnic_grp_tbl.ethnic_grp_cd = b_ed.ethnic_grp_cd		
and b_ed.edw_actv_ind='Y' and b_ed.edw_curr_ind='Y'		
and b_ed.effdt <= sysdate		
))
pivot (count(*) FOR ethnic_group IN (1 AS ethnic_group1,2 AS ethnic_group2 ,3 AS ethnic_group3,4 AS ethnic_group4,5 AS ethnic_group5,6 AS ethnic_group6,7 AS ethnic_group7) )

)

SELECT b.ACAD_YEAR ,b.CAL_YEAR ,b.SCHOOL_YEAR ,a.EMPLID ,a.STRM ,a.TERM_DESCRSHORT ,
a.ACAD_CAREER ,a.ACAD_LEVEL_BOT ,a.TERM_CLASSIF_CODE ,a.ENTRY_STATUS_CODE ,
a.RESIDENCY ,a.ADMISSION_RES ,a.TUITION_RES ,ENROLLMENT_STATUS ,
a.ACAD_PLAN ,a.ACAD_PROG , FIRST_PRIM_UGRD_FLAG ,FIRST_PRIM_CAREER ,
FALL_ENTERING_COHORT ,ACADEMIC_LOAD , nvl( crs.course_enrollments,0) AS course_enrollments, 
nvl(crs.assessed_credits,0) AS assessed_credits,
r.IPEDS_RACE_ETHNICITY ,r.IPEDS_CODE ,r.BIRTHDATE, r.SEX ,r.CITIZENSHIP_STATUS ,r.CITIZENSHIP_DESCRSHORT ,
r.RES_STATE ,r.RES_COUNTRY,r.RES_COUNTY ,
dis.c_special_need_os,dis.c_special_need_mobl,
dis.c_special_need_ac,dis.c_special_need_au, dis.c_special_need_brl, 
dis.c_special_need_lp,dis.c_special_need_nonm, pri.DEGREE, plan.ACAD_PLAN_DESCR ,plan.ACAD_PLAN_MAU , plan.ACAD_PLAN_MAU_DESCRFORMAL , plan.ACAD_PLAN_U1 , plan.ACAD_PLAN_U1_DESCRFORMAL ,
plan.EDUCATION_LVL ,plan.EDUCATION_LVL_XLATLONGNAME ,plan.CIP_CODE , nc.STEM_FLAG , eth.*
FROM siscs.R_STUDENTTERM_RV a
INNER JOIN siscs.R_TERM_RV b
ON a.strm=b.STRM 
AND a.INSTITUTION =b.INSTITUTION 
AND a.ACAD_CAREER =b.ACAD_CAREER 
left JOIN SISCS.R_PRIMACY_RV pri 
ON a.EMPLID =pri.EMPLID 
AND a.strm=pri.STRM 
AND a.ACAD_CAREER =pri.ACAD_CAREER 
AND a.INSTITUTION =pri.INSTITUTION 
AND a.ACAD_PLAN =pri.ACAD_PLAN 
LEFT JOIN crs 
ON a.EMPLID =crs.EMPLID 
AND a.ACAD_CAREER =crs.ACAD_CAREER 
AND a.INSTITUTION =crs.INSTITUTION 
AND a.strm =crs.strm
LEFT JOIN dis 
ON a.emplid = dis.emplid
LEFT JOIN siscs.R_STDNTBIODEMO_RV r 
ON a.EMPLID =r.EMPLID 
LEFT JOIN siscs.R_ACADPLAN_RV plan 
ON a.ACAD_PLAN = plan.ACAD_PLAN 
AND SYSDATE  BETWEEN plan.ACAD_PLAN_EFFDT  AND plan.ACAD_PLAN_EFFDT_END --might need TO MODIFY DATE here 
LEFT JOIN OPB_PERS_FALL.NCES_EXPANDED_V nc 
ON REPLACE(plan.CIP_CODE ,'.','')= nc.CIP_CODE 
LEFT JOIN eth 
ON a.emplid =eth.emplid
WHERE a.PRIMARY_CAR_FLAG ='Y'
AND a.strm =2218
AND a.Enrollment_Status IN ('E','W')

