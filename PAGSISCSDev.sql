
      
CREATE TABLE AWARDS
( "EMPLID" VARCHAR2(11 CHAR) NOT NULL,
  "ACAD_CAREER" VARCHAR2(4 CHAR) NOT NULL,
  "COMPLETION_TERM" VARCHAR2(4 CHAR),
  "ACAD_PLAN" VARCHAR2(10 CHAR),
  "DEGREE" VARCHAR2(8 CHAR),
  "EDUCATION_LVL" VARCHAR2(2 CHAR),
  "STDNT_DEGR" VARCHAR2(2 CHAR),
  "PLAN_SEQUENCE" VARCHAR2(2 CHAR),
  "MULTIAWARDSEQ" NUMBER,
  CONSTRAINT awards_pk PRIMARY KEY ("EMPLID", "ACAD_CAREER","STDNT_DEGR","ACAD_PLAN")
)

truncate table AWARDS
--drop table  AWARDS

insert into AWARDS
select aw.emplid, aw.acad_career1 as acad_career, aw.completion_term, aw.acad_plan,aw.degree,  aw.education_lvl,aw.stdnt_degr, aw.plan_sequence,
count(*) OVER( PARTITION BY aw.emplid, aw.acad_career1 ORDER BY
           aw.emplid, aw.acad_career1, aw.completion_term, aw.stdnt_degr, aw.plan_sequence
        ) AS multiawardseq
from (
select a.emplid,
a.stdnt_degr,
a.acad_career,
(  CASE WHEN EDUCATION_LVL = '17' THEN 'GM'
  WHEN EDUCATION_LVL = '21' THEN   'GD'
 WHEN  EDUCATION_LVL = 15 --first professional put in GD
  AND a.acad_career = 'GRRAD' THEN      'GD'   ELSE   a.acad_career  END ) AS acad_career1,
a.completion_term_descrshort,
a.completion_term,
a.acad_degr_status,
a.acad_plan,
a.ACAD_PLAN_DESCRSHORT,
a.degree,
--regexp_substr(a.acad_plan, '[^_]+$', 1, 1) as acad_plan_suffix, -- can be used to replace Permit_Rgstn_Flag?
a.EDUCATION_LVL,
b.plan_sequence,
a.acad_sub_plan,
count(*) over(partition by a.emplid, a.acad_career, a.stdnt_degr, a.completion_term, a.acad_plan order by a.emplid, a.acad_career, a.stdnt_degr, a.completion_term, a.acad_plan, a.acad_sub_plan) as cnt
from SISCS.R_STUDENTAWARD_RV a 
inner join SISCS.P_ACAD_PLAN_V b 
on a.emplid =b.emplid and a.acad_career=b.acad_Career
and a.completion_term=b.completion_term
and a.acad_plan=b.acad_plan
--and a.stdnt_degr=b.stdnt_degr
where  a.acad_degr_status='A' -- only include conferred awards
and a.completion_term >= 1759 -- award_term_seq_id >= '0754'
and  (a.degree in ( 'ATCT' , 'ELCT' , 'GRC2') or ( education_lvl>='13' and EDUCATION_LVL not in ( 'NO','NT') ) )
--and a.emplid in ('150555020','108471333','159425199')
) aw
where cnt=1

drop table PLVT_AWD1st
--truncate table PLVT_AWD1ST
CREATE TABLE PLVT_AWD1ST (
"EMPLID" VARCHAR2(11 CHAR) NOT NULL,
"STRM" VARCHAR2(4 CHAR),
"CLASS_CODE" VARCHAR2(3 CHAR),
"ACAD_CAREER" VARCHAR2(4 CHAR) NOT NULL,
"ENTRY_STATUS_CODE" VARCHAR2(4),
"TERM_CLASSIF_CODE" VARCHAR2(4),
"FIRST_PRIM_UGRD_FLAG" CHAR(1),
"FALL_ENTERING_COHORT" CHAR(1),
"ACADEMIC_LOAD" VARCHAR2(1 CHAR),
"ACAD_PLAN" VARCHAR2(10 CHAR),
"TERM_DESCRSHORT" VARCHAR2(10 CHAR),
 "COMPLETION_TERM" VARCHAR2(4 CHAR),
 "FIRST_AWARD_PLAN" VARCHAR2(10 CHAR)
 --CONSTRAINT plvt_award_pk PRIMARY KEY ("EMPLID", "ACAD_CAREER","STRM")
)

insert into PLVT_AWD1ST
select distinct plvt.*, awards.completion_term,awards.acad_plan as first_award_plan
from (
SELECT emplid,strm, (case when emplid ='107907383' and acad_career='GRAD' and acad_level_bot='P1' then 'PHD' else acad_level_bot end) as class_code,
(case when (case when emplid ='107907383' and acad_career='GRAD' and acad_level_bot='P1' then 'PHD' else acad_level_bot end) = 'MAS' THEN 'GM'
   WHEN (case when emplid ='107907383' and acad_career='GRAD' and acad_level_bot='P1' then 'PHD' else acad_level_bot end) ='PHD' THEN 'GD'
     ELSE ACAD_CAREER  END ) AS ACAD_CAREER,entry_status_code, term_classif_code, first_prim_ugrd_flag,fall_entering_cohort, academic_load, acad_plan, term_descrshort
      FROM
        siscs.r_studentterm_rv 
   WHERE
        PRIMARY_CAR_FLAG = 'Y'
        AND  ENROLLMENT_STATUS in ('E','W')
      --  and emplid  in ('152286916','126758953','127147277','120992970') --example cases
) plvt 
left join awards 
on plvt.emplid =awards.emplid
and plvt.acad_career= awards.acad_career and awards.multiawardseq=1


drop table Seqdat
--truncate table seqdat
CREATE TABLE Seqdat (
"EMPLID" VARCHAR2(11 CHAR) NOT NULL,
"STRM" VARCHAR2(4 CHAR),
"CLASS_CODE" VARCHAR2(3 CHAR),
"ACAD_CAREER" VARCHAR2(4 CHAR) NOT NULL,
"ENTRY_STATUS_CODE" VARCHAR2(4),
"TERM_CLASSIF_CODE" VARCHAR2(4),
"FIRST_PRIM_UGRD_FLAG" CHAR(1),
"FALL_ENTERING_COHORT" CHAR(1),
"ACADEMIC_LOAD" VARCHAR2(1 CHAR),
"ACAD_PLAN" VARCHAR2(10 CHAR),
"TERM_DESCRSHORT" VARCHAR2(10 CHAR),
 "COMPLETION_TERM" VARCHAR2(4 CHAR),
 "FIRST_AWARD_PLAN" VARCHAR2(10 CHAR),
 "ASCORDER"  NUMBER,
 "DESORDER" NUMBER,
 "PREVCLASSCODE" VARCHAR2(3 CHAR),
 "NEXTTERM" VARCHAR2(4 CHAR),
 "TERM_CNT" NUMBER,
 "SMTR_CNT" NUMBER,
 "TERM_CNT_1ST_DEGREE" NUMBER,
 "SMTR_CNT_1STDEGREE" NUMBER
-- CONSTRAINT Seqdat_pk PRIMARY KEY ("EMPLID", "ACAD_CAREER","STRM")
)

insert into Seqdat
SELECT  plvt_awd1st.* , COUNT(*) OVER(PARTITION BY emplid, acad_career ORDER BY emplid, acad_career   , strm )     AS ascorder
 , COUNT(*) OVER(PARTITION BY emplid, acad_career ORDER BY emplid, acad_career   , strm DESC)    AS desorder
 , LAG(class_code, 1 , '') OVER(PARTITION BY emplid, acad_career ORDER BY emplid, acad_career  ,strm   )          AS prevclasscode
  , LEAD(strm, 1   , '') OVER(PARTITION BY emplid, acad_career  ORDER BY emplid, acad_career , strm  )     AS nextterm
 , SUM(CASE WHEN strm < 1928 THEN 1 ELSE 0   END  ) OVER(PARTITION BY emplid, acad_career ORDER BY emplid, acad_career,strm  )       AS term_cnt
, SUM( CASE WHEN strm >= 1928 THEN 1 ELSE 0 END ) OVER(PARTITION BY emplid, acad_career ORDER BY emplid, acad_career,strm  )      AS smtr_cnt
 , SUM(CASE WHEN strm < 1928  AND strm <= completion_term THEN 1  ELSE 0     END  ) OVER(PARTITION BY emplid, acad_career  ORDER BY emplid, acad_career, strm )      AS term_cnt_1stdegree
   , SUM( CASE WHEN strm >= 1928 AND strm <= completion_term THEN 1 ELSE 0  END ) OVER(PARTITION BY emplid, acad_career  ORDER BY emplid, acad_career, strm) AS smtr_cnt_1stdegree
 FROM  plvt_awd1st
 
 truncate table pal_chrt
 CREATE TABLE  PAL_CHRT(
 "EMPLID" VARCHAR2(11 CHAR) NOT NULL,
"ACAD_CAREER" VARCHAR2(4 CHAR) NOT NULL,
 "SMTR_CNT" NUMBER,
 "TERM_CNT" NUMBER,
 "L_TERM" VARCHAR2(4 CHAR),
 "TERM_DESCRSHORT" VARCHAR2(10 CHAR),
 "SMTR_CNT_1STDEGREE" NUMBER,
 "TERM_CNT_1ST_DEGREE" NUMBER,
 "ENTRY_STATUS_CODE" VARCHAR2(4),
 "MAJOR_1ST" VARCHAR2(10 CHAR),
"F_TERM" VARCHAR2(4 CHAR),
"F_TERM_DESCRSHORT" VARCHAR2(10 CHAR),
"PAL_CHRT" VARCHAR2(4 CHAR),
"TERM_BEGIN_DT" DATE,
"F_CLASSCODE" VARCHAR2(3 CHAR),
"NEXTTERM" VARCHAR2(4 CHAR),
"SUMRFALL" VARCHAR2(2 CHAR),
"COMPLETION_TERM" VARCHAR2(4 CHAR),
"DEGR_YR" VARCHAR2(4 CHAR),
"FIRST_AWARD_PLAN" VARCHAR2(10 CHAR),
"JUNIOR_PLAN" VARCHAR2(10 CHAR),
"TTD_IN_YEARS" NUMBER(10,5),
  CONSTRAINT PAL_CHRT_pk PRIMARY KEY ("EMPLID", "ACAD_CAREER")
 )
 
 insert into PAL_CHRT
 select  ls.emplid , ls.acad_career , ls.smtr_cnt   , ls.term_cnt , ls.strm    AS l_term, ls.term_descrshort,
 ls.smtr_cnt_1stdegree     , ls.term_cnt_1st_degree ,f.entry_status_code, 
  f.acad_plan    AS major_1st, f.strm    AS f_term  , f.term_descrshort as f_term_descr, ftm.acad_year as pal_chrt, ftm.term_begin_dt, f.class_code    AS f_classcode,f.nextterm,
 (case when substr(f.nextterm,4,1)='8' and substr(f.strm,4,1)='5' and substr(f.strm,2,2)= substr(f.nextterm,2,2) then 'Y' else 'N' end) as sumrfall,
 ls.completion_term, tm.acad_year as degr_yr, ls.first_award_plan,
j.acad_plan as junior_plan,
 (tm.term_end_dt- ftm.term_begin_dt)/ 365.25 as ttd
from seqdat ls 
 inner join seqdat f 
 on ls.emplid=f.emplid
 and ls.acad_career=f.acad_career
left join SISCS.s_term_tbl_v tm
on (case when  ls.acad_career in ('GM','GD') then 'GRAD' else ls.acad_career end) =tm.acad_career
and ls.completion_term = tm.strm
 inner join SISCS.s_term_tbl_v ftm
 on (case when  f.acad_career in ('GM','GD') then 'GRAD' else f.acad_career end) =ftm.acad_career
and f.strm = ftm.strm
  and f.ascorder = 1
left join (SELECT emplid, acad_career , acad_plan , COUNT(*) OVER(PARTITION BY emplid, acad_career   ORDER BY     emplid, acad_career ,strm DESC   ) AS juntermdesc
from seqdat
WHERE   prevclasscode <> '30'
AND class_code = '30') j
on j.juntermdesc=1
and j.emplid = ls.emplid
and j.acad_career= ls.acad_career
where ls.desorder=1

truncate table PAG_MATRICS_LONG
 CREATE TABLE  PAG_MATRICS_LONG (
 "EMPLID" VARCHAR2(11 CHAR) NOT NULL,
"ACAD_CAREER"  VARCHAR2(4 CHAR) NOT NULL,
"ENTRY_STRM"  VARCHAR2(4 CHAR),
"STRM"  VARCHAR2(4 CHAR) ,
"TERM_DESCRSHORT" VARCHAR2(10 CHAR),
"PAL_CHRT" VARCHAR2(4 CHAR),
"SUMRFALL" VARCHAR2(2 CHAR),
"INDEX_OLD" NUMBER,
"INDEX_NEW" NUMBER,
"TERM_SEQ_COUNTER" NUMBER,
"CLASS_CODE" VARCHAR2(3 CHAR),
"ENTRY_STATUS_CODE" VARCHAR2(4),
"ACAD_PLAN" VARCHAR2(10 CHAR),
"COMPLETION_TERM" VARCHAR2(4 CHAR),
"MATRIC" NUMBER,
"M" NUMBER,
 CONSTRAINT PAG_MATRICS_LONG_pk PRIMARY KEY ("EMPLID", "ACAD_CAREER","STRM")
 )
 
 insert into PAG_MATRICS_LONG
  select a.emplid, a.acad_career, a.F_term as entry_strm,t.strm,t.descrshort as term_descrshort,pal_chrt,SUMRFALL,
  ((case when t.strm<2000 then '19'||substr(t.descrshort,3,2) else '20'||substr(t.descrshort,3,2) end) - pal_chrt) as index_old,
((case when  t.strm<2000 then 1900+ (case when substr(t.descrshort,1,1) in ('S','Q') then to_number( substr(t.strm,2,2))-1 else  to_number( substr(t.strm,2,2)) end   ) else 2000+ (case when substr(t.descrshort,1,1) in ('S','Q') then to_number( substr(t.strm,2,2))-1 else  to_number( substr(t.strm,2,2)) end   ) end) - pal_chrt+1) as index_new,
 count(*) over (partition by a.emplid, a.acad_career order by a.emplid, a.acad_career, a.F_term,t.strm) as termcounter,
p.class_code, p.entry_status_code, p.acad_plan,a.completion_term,
( CASE WHEN a.completion_term < t.strm THEN 2
   WHEN p.emplid IS NULL THEN 0 ELSE 1 END ) AS matric  , 
 ( CASE WHEN a.completion_term =t.strm        THEN 3
WHEN a.completion_term < t.strm        THEN 2
WHEN p.emplid  IS NULL THEN 0 ELSE 1 END ) AS m
 from pal_chrt a
 inner join siscs.s_term_tbl_v t 
 on a.acad_career=t.acad_career
 left join plvt_awd1st p
 on a.emplid=p.emplid
 and a.acad_career=p.acad_career
 and t.strm = p.strm
 where t.strm>=a.f_term
 and t.term_begin_dt <=sysdate
 --and a.emplid  in ('152286916','126758953','127147277','120992970')
 order by a.emplid, a.acad_career, a.F_term,t.strm
 
 
 
--drop table PAG_FLAT
create table PAG_FLAT as (
select a.emplid,a.acad_career,a.pal_chrt as cohort,sumrfall as ENTRANT_SUMMER_FALL,
a.persist1, a.persist2, a.persist3, a.persist4, a.persist5, a.persist6, a.persist7, a.persist8,
a.persist_sp1, a.persist_sp2, a.persist_sp3, a.persist_sp4, a.persist_sp5, a.persist_sp6, a.persist_sp7, a.persist_sp8,
a.persist_su1, a.persist_su2, a.persist_su3, a.persist_su4, a.persist_su5, a.persist_su6, a.persist_su7, a.persist_su8,
b.grad1,b.grad2,b.grad3,b.grad4, b.grad5, b.grad6,b.grad7,b.grad8,
b.grad_sp1,b.grad_sp2,b.grad_sp3,b.grad_sp4, b.grad_sp5, b.grad_sp6,b.grad_sp7,b.grad_sp8,
b.grad_su1,b.grad_su2,b.grad_su3,b.grad_su4, b.grad_su5, b.grad_su6,b.grad_su7,b.grad_su8,
c.matric1,c.matric2,c.matric3,c.matric4, c.matric5, c.matric6,c.matric7,c.matric8,
c.matric_sp1,c.matric_sp2,c.matric_sp3,c.matric_sp4, c.matric_sp5, c.matric_sp6,c.matric_sp7,c.matric_sp8,
c.matric_su1,c.matric_su2,c.matric_su3,c.matric_su4, c.matric_su5, c.matric_su6,c.matric_su7,c.matric_su8,
d.M_US1,d.M_US2,d.M_US3, d.M_US4,d.M_US5,d.M_US6,d.M_US7, d.M_US8,d.M_US9,
d.M_FS1,d.M_FS2,d.M_FS3, d.M_FS4,d.M_FS5,d.M_FS6,d.M_FS7, d.M_FS8,d.M_FS9,
d.M_SS1,d.M_SS2,d.M_SS3, d.M_SS4,d.M_SS5,d.M_SS6,d.M_SS7, d.M_SS8,d.M_SS9,
e.P_US1,e.P_US2,e.P_US3, e.P_US4,e.P_US5,e.P_US6,e.P_US7, e.P_US8,e.P_US9,
e.P_FS1,e.P_FS2,e.P_FS3, e.P_FS4,e.P_FS5,e.P_FS6,e.P_FS7, e.P_FS8,e.P_FS9,
e.P_SS1,e.P_SS2,e.P_SS3, e.P_SS4,e.P_SS5,e.P_SS6,e.P_SS7, e.P_SS8,e.P_SS9,
f.G_US1,f.G_US2,f.G_US3, f.G_US4,f.G_US5,f.G_US6,f.G_US7, f.G_US8,f.G_US9,
f.G_FS1,f.G_FS2,f.G_FS3, f.G_FS4,f.G_FS5,f.G_FS6,f.G_FS7, f.G_FS8,f.G_FS9,
f.G_SS1,f.G_SS2,f.G_SS3, f.G_SS4,f.G_SS5,f.G_SS6,f.G_SS7, f.G_SS8,f.G_SS9
from
(select *  from (select 
emplid, acad_career, pal_chrt, sumrfall,  (case when MATRIC>=1 then 100 else 0 end) as Persist,
(case when substr(term_descrshort,1,1)='F' then to_char(index_old) when substr(term_descrshort,1,1)='S' then 'SP'||index_old when substr(term_descrshort,1,1)='U' then 'SU'||index_old end) as type
from PAG_MATRICS_LONG
where index_old >=1
--and emplid  in ('152286916','126758953','127147277','120992970')
)
pivot ( 
  max(Persist) for type in ( 
   '1' persist1,'SP1' persist_sp1,'SU1' persist_su1, 
   '2' persist2,'SP2' persist_sp2,'SU2' persist_su2,
   '3' persist3,'SP3' persist_sp3,'SU3' persist_su3,
   '4' persist4,'SP4' persist_sp4,'SU4' persist_su4,
   '5' persist5,'SP5' persist_sp5,'SU5' persist_su5,
   '6' persist6,'SP6' persist_sp6,'SU6' persist_su6,
   '7' persist7,'SP7' persist_sp7,'SU7' persist_su7,
   '8' persist8,'SP8' persist_sp8,'SU8' persist_su8)
)
) a
left join 
(select *  from (select 
emplid, acad_career,  (case when MATRIC>=2 then 100 else 0 end) as GRAD,
(case when substr(term_descrshort,1,1)='F' then to_char(index_old) when substr(term_descrshort,1,1)='S' then 'SP'||index_old when substr(term_descrshort,1,1)='U' then 'SU'||index_old end) as type
from PAG_MATRICS_LONG
where index_old >=1)
pivot ( 
  max(GRAD) for type in ( 
   '1' GRAD1,'SP1' GRAD_sp1,'SU1' GRAD_su1, 
   '2' GRAD2,'SP2' GRAD_sp2,'SU2' GRAD_su2,
   '3' GRAD3,'SP3' GRAD_sp3,'SU3' GRAD_su3,
   '4' GRAD4,'SP4' GRAD_sp4,'SU4' GRAD_su4,
   '5' GRAD5,'SP5' GRAD_sp5,'SU5' GRAD_su5,
   '6' GRAD6,'SP6' GRAD_sp6,'SU6' GRAD_su6,
   '7' GRAD7,'SP7' GRAD_sp7,'SU7' GRAD_su7,
   '8' GRAD8,'SP8' GRAD_sp8,'SU8' GRAD_su8)
)) b
on a.emplid =b.emplid and a.acad_career=b.acad_career
left join 
(select *  from (select 
emplid, acad_career,  MATRIC,
(case when substr(term_descrshort,1,1)='F' then to_char(index_old) when substr(term_descrshort,1,1)='S' then 'SP'||index_old when substr(term_descrshort,1,1)='U' then 'SU'||index_old end) as type
from PAG_MATRICS_LONG
where index_old >=1)
pivot ( 
  max(MATRIC) for type in ( 
   '1' MATRIC1,'SP1' MATRIC_sp1,'SU1' MATRIC_su1, 
   '2' MATRIC2,'SP2' MATRIC_sp2,'SU2' MATRIC_su2,
   '3' MATRIC3,'SP3' MATRIC_sp3,'SU3' MATRIC_su3,
   '4' MATRIC4,'SP4' MATRIC_sp4,'SU4' MATRIC_su4,
   '5' MATRIC5,'SP5' MATRIC_sp5,'SU5' MATRIC_su5,
   '6' MATRIC6,'SP6' MATRIC_sp6,'SU6' MATRIC_su6,
   '7' MATRIC7,'SP7' MATRIC_sp7,'SU7' MATRIC_su7,
   '8' MATRIC8,'SP8' MATRIC_sp8,'SU8' MATRIC_su8)
)) c
on a.emplid=c.emplid and a.acad_career=c.acad_career
left join 
(select *  from (select 
emplid, acad_career,  M,
(substr(term_descrshort,1,1)||'S'||index_new ) as type
from PAG_MATRICS_LONG
where index_new <=10)
pivot ( 
  max(M) for type in ( 
   'FS1' M_FS1,'SS1' M_SS1,'US1' M_US1, 
   'FS2' M_FS2,'SS2' M_SS2,'US2' M_US2,
   'FS3' M_FS3,'SS3' M_SS3,'US3' M_US3,
   'FS4' M_FS4,'SS4' M_SS4,'US4' M_US4,
   'FS5' M_FS5,'SS5' M_SS5,'US5' M_US5,
   'FS6' M_FS6,'SS6' M_SS6,'US6' M_US6,
   'FS7' M_FS7,'SS7' M_SS7,'US7' M_US7,
   'FS8' M_FS8,'SS8' M_SS8,'US8' M_US8,
   'FS9' M_FS9,'SS9' M_SS9,'US9' M_US9)
)) d
on a.emplid=d.emplid and a.acad_career=d.acad_career
left join 
(select *  from (select 
emplid, acad_career, (case when M>=1 then 100 else 0 end) as P ,
(substr(term_descrshort,1,1)||'S'||index_new ) as type
from PAG_MATRICS_LONG
where index_new <=10)
pivot ( 
  max(P) for type in ( 
   'FS1' P_FS1,'SS1' P_SS1,'US1' P_US1, 
   'FS2' P_FS2,'SS2' P_SS2,'US2' P_US2,
   'FS3' P_FS3,'SS3' P_SS3,'US3' P_US3,
   'FS4' P_FS4,'SS4' P_SS4,'US4' P_US4,
   'FS5' P_FS5,'SS5' P_SS5,'US5' P_US5,
   'FS6' P_FS6,'SS6' P_SS6,'US6' P_US6,
   'FS7' P_FS7,'SS7' P_SS7,'US7' P_US7,
   'FS8' P_FS8,'SS8' P_SS8,'US8' P_US8,
   'FS9' P_FS9,'SS9' P_SS9,'US9' P_US9)
)) e
on a.emplid=e.emplid and a.acad_career=e.acad_career
left join 
(select *  from (select 
emplid, acad_career, (case when M>=2 then 100 else 0 end) as G ,
(substr(term_descrshort,1,1)||'S'||index_new ) as type
from PAG_MATRICS_LONG
where index_new <=10)
pivot ( 
  max(G) for type in ( 
    'FS1' G_FS1,'SS1' G_SS1,'US1' G_US1, 
   'FS2' G_FS2,'SS2' G_SS2,'US2' G_US2,
   'FS3' G_FS3,'SS3' G_SS3,'US3' G_US3,
   'FS4' G_FS4,'SS4' G_SS4,'US4' G_US4,
   'FS5' G_FS5,'SS5' G_SS5,'US5' G_US5,
   'FS6' G_FS6,'SS6' G_SS6,'US6' G_US6,
   'FS7' G_FS7,'SS7' G_SS7,'US7' G_US7,
   'FS8' G_FS8,'SS8' G_SS8,'US8' G_US8,
   'FS9' G_FS9,'SS9' G_SS9,'US9' G_US9)
)) f
on a.emplid=f.emplid and a.acad_career=f.acad_career
)

